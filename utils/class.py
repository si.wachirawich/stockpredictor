from cluster import *

		
if __name__ == '__main__':
	data_frame = 30
	dtw_threshold = 1.40
	config = Config('Close', data_frame, dtw_threshold, ConfigPath(data_frame = data_frame)) 
	sc = StockCluster(config) 
	r_X, r_y = sc.prepare_predict_data()

	X = r_X.reshape((r_X.shape[0], r_X.shape[1], 1))
	# model = sc.lstm_reg_model(X=X, y=r_y, new=False)
	# sc.display_reg_result(model, r_X, r_y)
	# # sp.load_data()
	# # d = sp.prepare_data()
	# # sp.run_cluster()
	# # sp.test_cluster()
	
	# # pprint.pprint(sp.cluster_data)
	# # sp.visualize_cluster(show=False, save=True)

	sc.load_cluster()
	c_X, c_y = sc.prepare_training_set()
	model = sc.lstm_cls_model(X=X, y=c_y, new=False)
	sc.display_cls_result(model, c_X, c_y)
	