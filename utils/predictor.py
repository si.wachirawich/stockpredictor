import cluster 
import os
import numpy as np
import matplotlib as mp 
import os
import time
import matplotlib.pyplot as plt
from keras.utils import to_categorical
from keras.models import Sequential, model_from_json
from keras.layers import Dense, Activation, LSTM
from keras.utils import to_categorical
EPOCHS = 50

class ConfigPath:
	def __init__(self, reg_model=None, reg_weight=None \
			, cls_model=None, cls_weight=None):

		self.file_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

		self.snap_path = os.path.join(self.file_path, 'snapshots') 
		if not os.path.exists( self.snap_path ):
			os.mkdir(self.snap_path)

		if reg_model:
			self.reg_model = reg_model 
		else:
			self.reg_model = os.path.join(self.snap_path, 'reg_model') 
		if not os.path.exists( self.reg_model ):
			os.mkdir(self.reg_model)

		if reg_weight:
			self.reg_weight = reg_weight
		else:
			self.reg_weight = os.path.join(self.snap_path, 'reg_weight')
		if not os.path.exists( self.reg_weight ):
			os.mkdir(self.reg_weight)

		if cls_model:
			self.cls_model = cls_model
		else:
			self.cls_model = os.path.join(self.snap_path, 'cls_model')
		if not os.path.exists( self.cls_model ):
			os.mkdir(self.cls_model)

		if cls_weight:
			self.cls_weight = cls_weight
		else:
			self.cls_weight = os.path.join(self.snap_path, 'cls_weight')           
		if not os.path.exists( self.cls_weight ):
			os.mkdir(self.cls_weight)



class StockPredictor():
	def __init__(self, config_path):
		self.path   = config_path
		self.model_reg = None
		self.model_cls = None

	def lstm_reg_model(self, X, y, new=True):
		if new:
			model = Sequential([
				LSTM(128,input_shape=(25,1), return_sequences=True),
				LSTM(64,input_shape=(128,1)),
				Dense(5)
			])
			model.compile(loss='mse', optimizer='adam')
			time_start = time.time()
			model.fit(X, y, epochs=EPOCHS)
			print("Training Time : %d" % (time.time()-time_start))
			model_json = model.to_json()
			t= time.strftime("%Y%m%d%H%M")
			save_model_file = os.path.join(self.path.reg_model, t+'_reg_model')
			cluster.save_obj(model_json, save_model_file)
			save_weight_file = os.path.join(self.path.reg_weight, t+'_reg_weight.h5')
			model.save_weights(save_weight_file)
		else:
		
			last_weight_file = sorted(os.listdir(self.path.reg_weight))[-1]
			last_model_file = [i for i in os.listdir(self.path.reg_model) if i[:12] == last_weight_file[:12] ][0]
			last_model_file = os.path.join(self.path.reg_model, last_model_file)
			loaded_model_json = cluster.load_obj(last_model_file)
			model = model_from_json(loaded_model_json)


			obj_name = os.path.join(self.path.reg_weight, last_weight_file)
			model.load_weights(obj_name)

			
			model.compile(loss='mse', optimizer='adam')

		self.model_reg = model
		return model

	def lstm_cls_model(self, X, y, new=True):
		if new:
			model = Sequential([
				LSTM(128,input_shape=(30,1), return_sequences=True),
				LSTM(64,input_shape=(128,1)),
				Dense(y.shape[1]),
				Activation('sigmoid')
			])
			model.compile(loss='mse', optimizer='adam')
			time_start = time.time()
			model.fit(X, y, epochs=EPOCHS)
			print("Training Time : %d" % (time.time()-time_start))
			model_json = model.to_json()
			t= time.strftime("%Y%m%d%H%M")
			save_model_file = os.path.join(self.path.cls_model, t+'_cls_model')
			cluster.save_obj(model_json, save_model_file)
			save_weight_file = os.path.join(self.path.cls_weight, t+'_cls_weight.h5')
			model.save_weights(save_weight_file)
		else:
			last_weight_file = sorted(os.listdir(self.path.cls_weight))[-1]
	
			last_model_file = [i for i in os.listdir(self.path.cls_model) if i[:12] == last_weight_file[:12]][0] 
			last_model_file = os.path.join(self.path.cls_model, last_model_file)
			loaded_model_json = cluster.load_obj(last_model_file)
			model = model_from_json(loaded_model_json)

			obj_name = os.path.join(self.path.cls_weight, last_weight_file)
			model.load_weights(obj_name)

			model.compile(loss='mse', optimizer='adam')

		self.model_cls = model
		return model

	def display_reg_result(self, model, r_X, r_y):
		plt.clf() 
		i = 951
		X = r_X[i].reshape((1, 25, 1))
		yhat = model.predict(X)
		a = r_X[i]
		b =  yhat[0]
		c = (np.append(a,b))
		d = (np.append(a,r_y[i]))
		plt.plot(range(30), c, '.-')
		plt.plot(range(30), d)
		plt.show()


	def display_cls_result(self, model, X, y):
		X = X[500].reshape((1, 30, 1))
		yhat = model.predict(X)
		print(yhat)
		print(max(yhat[0]))

if __name__ == '__main__':
	data_frame = 30
	dtw_threshold = 1.40
	config = cluster.Config('Close', data_frame, dtw_threshold, cluster.ConfigPath(data_frame = data_frame)) 
	sc = cluster.StockCluster(config)
	# sp.load_data()
	# d = sp.prepare_data()
	# sp.run_cluster()
	# sp.test_cluster()
	sc.load_cluster()

	sp = StockPredictor(ConfigPath())
	# pprint.pprint(sp.cluster_data)
	# sp.visualize_cluster(show=False, save=True)
	r_X, r_y, c_X, c_y = sc.prepare_training_set()

	# X = r_X.reshape((r_X.shape[0], r_X.shape[1], 1))
	# model = sp.lstm_reg_model(X=X, y=r_y, new=True)
	# sp.display_reg_result(model, r_X, r_y)

	X = c_X.reshape((c_X.shape[0], c_X.shape[1], 1))
	print((c_y.shape[0]))
	model = sp.lstm_cls_model(X=X, y=c_y, new=False)
	sp.display_cls_result(model, c_X, c_y)
	