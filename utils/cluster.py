import pandas as pd
import numpy as np
import matplotlib as mp 
import pprint
import pickle
import os
import time
import matplotlib.pyplot as plt
from cdtw import pydtw
from sklearn.preprocessing import normalize, MinMaxScaler
from scipy.spatial.distance import euclidean
from scipy.spatial import distance
from keras.utils import to_categorical
from keras.models import Sequential, model_from_json
from keras.layers import Dense, Activation, LSTM, Dropout
from keras.utils import to_categorical

norm = MinMaxScaler(feature_range = (0, 1))
EPOCHS = 100
def save_obj(obj, name ):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open(name, 'rb') as f:
        return pickle.load(f)

def dtw(s1, s2):
    # return pydtw.dtw(s1,s2).get_dist()
    return distance.euclidean(s1, s2)

class StockCluster():
    def __init__(self, config):
        self.data_frame = config.data_frame
        self.path = config.path
        self.dtw_threshold = config.dtw_threshold
        self.price = config.price
        self.dict_data = {}
        self.cluster_data = {}
        self.model_reg = None
        self.model_cls = None

    def prepare_data(self, obj_name=None , data_frame=None ,dataset_type=None):
        if obj_name == None:
            obj_name  = self.path.dataset_path
        if data_frame == None:
            data_frame = self.data_frame
        # prepare format data
        data_l = os.listdir(obj_name)
        dict_data = { } 
        for p in range(0, len(data_l)):
            
            data = pd.read_csv(os.path.join(obj_name, data_l[p]) )
            data.dropna(inplace = True)
            iter_round = ( data.shape[0] // data_frame ) - 1
            dict_row =  len(dict_data.keys())
            print(data_l[p], data.shape[0], dict_row)
            for sp in range(0,data_frame-1, data_frame//6):
                for t in range(iter_round):
                    s_t = iter_round*sp + t + dict_row
                    dict_data[s_t] = {}
                    dict_data[s_t]['name'] = os.path.splitext(data_l[p])[0]
                    s = t*data_frame + sp
                    d = (data.loc[s:s+data_frame-1, :]).to_dict()
                    
                    for k in d.keys():
                        dict_data[s_t][k] = (np.array(list(d[k].values())) )

        self.dict_data = dict_data
        t= time.strftime("%Y%m%d%H%M")
        save_obj(dict_data, os.path.join(self.path.prepared_path,'chunk_stock_'+ \
            str(self.data_frame)+'_'+t))
        # pprint.pprint(dict_data)
        return dict_data

    def prepare_predict_data(self, obj_name=None , data_frame=None ,dataset_type=None):
        
        length = (self.data_frame//6)*5
        r_X, r_y = [], []

        if obj_name == None:
            obj_name  = self.path.dataset_path
        if data_frame == None:
            data_frame = self.data_frame
        # prepare format data
        data_l = os.listdir(obj_name)
        time_start = time.time()
        
        for p in range(0, len(data_l)):
            # loop for file in folder
            data = pd.read_csv(os.path.join(obj_name, data_l[p]) )
            data.dropna(inplace = True)
            data = data.loc[data.Date > '2010-01-00']
            d = data[self.price].values
            d = norm.fit_transform(d[:,np.newaxis]).ravel()
            # print(type(d), (d.shape))
            print(d)
            for i in range(0,len(d)-data_frame,data_frame):
                # print(d[i:i+length])
                # print(d[i+length:i+data_frame])
                r_X.append(d[i:i+length])
                r_y.append(d[i+length:i+data_frame])
            # print(r_X)
            # for i
            # for i in range(start_row, len(d), data_frame):
            #     # loop for data stide
            #     # d = (data.loc[i:i+data_frame-1, :]).to_dict()
                
                
            #     # get data chuck
            #     # for p in d[self.price].values():
            #         # get value price 
            #     price = np.append(price, p)
            # price = norm.fit_transform(price[:,np.newaxis]).ravel()
            # if len(price) == 30:
            #     r_X.append(price[:length])
            #     r_y.append(price[length:])
            
        print(np.array(r_X).shape, np.array(r_y).shape)
        print("Prepareing data set for prediction Time : %d" % (time.time()-time_start))
        return np.array(r_X), np.array(r_y)

    def load_data(self, obj_name=None):
        if obj_name == None: 
        # if None get latest
            obj_name = os.path.join(self.path.prepared_path, \
            sorted(os.listdir(self.path.prepared_path))[-1])
        print ('Load '+obj_name)
        self.dict_data = load_obj(obj_name)
        return self.dict_data

    def load_cluster(self, obj_name=None):
        if obj_name == None: 
        # if None get latest
            obj_name = os.path.join(self.path.cluster_path, \
            sorted(os.listdir(self.path.cluster_path))[-1])
        print ('Load '+obj_name)
        self.cluster_data = load_obj(obj_name)
        return self.cluster_data

    def find_conn(self, price_list):
        dirt = [0,0]
        data = price_list[1] - price_list[0]
        for i in range(1, self.data_frame-1):
            data_t = price_list[i+1] - price_list[i]
            # print('{0} - {1} = {2} {3}'.format(price_list[i+1], price_list[i], data_t, data_t-data))
            if data_t - data > 0.004:
                dirt[1] += 1
            elif data_t - data < -0.004:
                dirt[0] += 1
            data = data_t

        return dirt

    def test_cluster(self):
        dict_data = self.dict_data
        l1 =  list(dict_data.keys())
        l2 = l1
        cluster_data = { } 
        clss = 0
        print('Total data: ', len(l1))
        k = 9999

        s1 = (dict_data[k][self.price])
        s1_norm = norm.fit_transform(s1[:,np.newaxis]).ravel()
        print(s1_norm)
        l2.remove(k)
        # loop for comp element
        # threshold = [0.004, 0.005, 0.006, 0.007, 0.008]
      
        threshold = np.arange(1.4, 1.5, 0.02)
        th_l = len(threshold)
        result = [[] for i in range(th_l)]
        print((th_l))
        for i in l2:
            s2 = (dict_data[i][self.price])
            s2_norm = norm.fit_transform(s2[:,np.newaxis]).ravel()
            if True:
                d = pydtw.dtw(s1_norm, s2_norm).get_dist()
                for t in range(1, th_l+1):
                    if ( d < threshold[t-1] ):
                        result[t-1].append(s2_norm)
                        
        # print(result[1][2])
        for h in range(1, th_l+1):    
            plt.subplot(th_l, 1, h)  
            for i in result[h-1]:
                plt.plot(range(self.data_frame), i, '.-')          
            plt.title('Cluster threshold '+ str(threshold[h-1]))
        plt.show()

        self.cluster_data = cluster_data
        return cluster_data          

    def run_cluster(self):
        dict_data = self.dict_data
        l1 =  list(dict_data.keys())
        l2 = l1
        cluster_data = { } 
        clss = 0
        print('Total data: ', len(l1))
        # loop for main element
        for k in l1:
            # plt.plot(range(0,7), s1_norm)
            print(k, len(l2))
            s1 = (dict_data[k][self.price])
            # s1_norm = (normalize(s1[:,np.newaxis], axis=0).ravel())
            s1_norm = norm.fit_transform(s1[:,np.newaxis]).ravel()
            # s1_dirt = self.find_conn(s1_norm)
            cluster_data[clss] = { }
            l2.remove(k)
            # loop for comp element
            for i in l2:
                s2 = (dict_data[i][self.price])
                # s2_norm = (normalize(s2[:,np.newaxis], axis=0).ravel()) 
                s2_norm = norm.fit_transform(s2[:,np.newaxis]).ravel()
                # s2_dirt = self.find_conn(s2_norm)
                # if  s1_dirt == s2_dirt:
                if True:
                    d = pydtw.dtw(s1_norm, s2_norm) .get_dist()
                    if ( d < self.dtw_threshold ):
                        cluster_data[clss][i] = dict_data[i]
                        l2.remove(i)
                        # plt.plot(range(0,7), s2_norm , linestyle='dashed')
            if len(cluster_data[clss]) != 0:
                cluster_data[clss][k] = dict_data[k]
                clss += 1
            else:
                del( cluster_data[clss] )
        t= time.strftime("%Y%m%d%H%M")
        save_obj(cluster_data, os.path.join(self.path.cluster_path,'clutering_'+ \
            str(self.data_frame)+'_'+t))
        self.cluster_data = cluster_data
        return cluster_data   

    def visualize_cluster(self, show=True, save=False ):
        l1 = len(list(self.cluster_data.keys()))
        for r in range(20):
            row = 1
            plt.figure(figsize=(16,12))
            for i in np.random.randint(1,l1, size=5):
                plt.subplot(5, 1, row)
                plt.ylabel('Cluster: {0}'.format(i))
                for j in list(self.cluster_data[i].keys()):
                    s = self.cluster_data[i][j][self.price]
                    # s = (normalize(s[:,np.newaxis], axis=0).ravel()) 
                    s = norm.fit_transform(s[:,np.newaxis]).ravel()
                    plt.plot(range(self.data_frame), s, '.-')
                
                if row == 1:
                    plt.title('Cluster threshold '+ str(self.dtw_threshold))
                row += 1
            plt.xlabel('time (d)')
            
            if save:
                plt.savefig(os.path.join(self.path.img_path, 'd_'+ str(self.data_frame) +'_cluster_' \
                        +str(r).zfill(2)+'.png'))
            if show:
                plt.show()
            plt.clf() 
    
    def prepare_training_set(self):
        length = (self.data_frame//6)*5
        r_X, r_y, c_X, c_y = [], [], [], []
        for i in list(self.cluster_data.keys()):
            for j in list(self.cluster_data[i].keys()):
                data = self.cluster_data[i][j][self.price]
                c_X.append(norm.fit_transform(data[:,np.newaxis]).ravel())
                c_y.append(i)
        return np.array(c_X), to_categorical(np.array(c_y))

    def display_training_set(self, r_X, r_y, c_X, c_y):
        for i in range(len(r_X))[:10]:
            for j in range(len(r_X[i])):
                if j < len(r_y[i]) - 1:
                    print (r_X[i][j], r_y[i][j])
                else:
                    print (r_X[i][j])

        for i in range(len(c_X))[:10]:
            for j in range(len(c_X[i])):
                if j < len(c_y) - 1:
                    print (c_X[i][j], c_y[i])
                else:
                    print (c_X[i][j])

    def lstm_reg_model(self, X, y, new=True):
        if new:
            model = Sequential([
                LSTM(100,input_shape=(25,1), return_sequences=True),
                Dropout(0.2),
                LSTM(100,input_shape=(100,1), return_sequences=True),
                Dropout(0.2),
                LSTM(100,input_shape=(100,1), return_sequences=True),
                Dropout(0.2),
                LSTM(100,input_shape=(100,1)),
                Dropout(0.2),
                Dense(5)
            ])
            model.compile(loss='mean_squared_error', optimizer='adam')
            time_start = time.time()
            model.fit(X, y, epochs=EPOCHS, batch_size = 32)
            print("Training Time : %d" % (time.time()-time_start))
            model_json = model.to_json()
            t= time.strftime("%Y%m%d%H%M")
            save_model_file = os.path.join(self.path.reg_model, t+'_reg_model')
            save_obj(model_json, save_model_file)
            save_weight_file = os.path.join(self.path.reg_weight, t+'_reg_weight.h5')
            model.save_weights(save_weight_file)
        else:
            last_weight_file = sorted(os.listdir(self.path.reg_weight))[-1]
            last_model_file = [i for i in os.listdir(self.path.reg_model) if i[:12] == last_weight_file[:12] ][0]
            last_model_file = os.path.join(self.path.reg_model, last_model_file)
            loaded_model_json = load_obj(last_model_file)
            model = model_from_json(loaded_model_json)

            obj_name = os.path.join(self.path.reg_weight, last_weight_file)
            model.load_weights(obj_name)
            model.compile(loss='mse', optimizer='adam')

        self.model_reg = model
        return model

    def lstm_cls_model(self, X, y, new=True):
        if new:
            model = Sequential([
                LSTM(128,input_shape=(30,1), return_sequences=True),
                LSTM(64,input_shape=(128,1)),
                Dense(y.shape[1]),
                Activation('sigmoid')
            ])
            model.compile(loss='mse', optimizer='adam')
            time_start = time.time()
            model.fit(X, y, epochs=EPOCHS)
            print("Training Time : %d" % (time.time()-time_start))
            model_json = model.to_json()
            t= time.strftime("%Y%m%d%H%M")
            save_model_file = os.path.join(self.path.cls_model, t+'_cls_model')
            save_obj(model_json, save_model_file)
            save_weight_file = os.path.join(self.path.cls_weight, t+'_cls_weight.h5')
            model.save_weights(save_weight_file)
        else:
            last_weight_file = sorted(os.listdir(self.path.cls_weight))[-1]

            last_model_file = [i for i in os.listdir(self.path.cls_model) if i[:12] == last_weight_file[:12]][0] 
            last_model_file = os.path.join(self.path.cls_model, last_model_file)
            loaded_model_json = load_obj(last_model_file)
            model = model_from_json(loaded_model_json)

            obj_name = os.path.join(self.path.cls_weight, last_weight_file)
            model.load_weights(obj_name)

            model.compile(loss='mse', optimizer='adam')

        self.model_cls = model
        return model

    def display_reg_result(self, model, r_X, r_y, save=True, show=False):
       
        plt.figure(figsize=(16,12))
        for i in np.random.randint(1,r_X.shape[0], 20):
            X = r_X[i].reshape((1, 25, 1))
            yhat = model.predict(X)
            a = r_X[i]
            b = yhat[0]
            c = (np.append(a,b))
            d = (np.append(a,r_y[i]))
            c = norm.inverse_transform(c[:,np.newaxis]).ravel()
            d = norm.inverse_transform(d[:,np.newaxis]).ravel()
            plt.plot(range(30), c, '.-')
            plt.plot(range(30), d)
            plt.xlabel('time (d)')
            
            if save:
                plt.savefig(os.path.join(self.path.img_path, 'd_'+ str(self.data_frame) +'_predict_' \
                        +str(i).zfill(2)+'.png'))
            if show:
                plt.show()
            plt.clf() 

    def display_cls_result(self, model, X, y):
        X_l = X[1000].reshape((1, 30, 1))
        yhat = model.predict(X_l)
        print(yhat)
        print(max(yhat[0])*100) 
        i = np.where(yhat[0] == max(yhat[0]))[0][0]
        print(i)
        data = self.cluster_data[i]
        print(data)
        plt.figure(figsize=(16,12))
        i = list(data.keys())[0]
        c = (data[i][self.price])
        d = X[1000]
        plt.subplot(211)
        plt.plot(range(30), c, '.-')
        plt.subplot(212)
        plt.plot(range(30), d)
        plt.xlabel('time (d)')
        plt.show()
        
            


        
class Config():
    def __init__(self, price, data_frame, dtw_threshold, configPath):
        self.data_frame = data_frame
        self.path = configPath
        self.price = price
        self.dtw_threshold = dtw_threshold

class ConfigPath():
    def __init__(self, data_frame, file_path=None, source_path=None \
            , dataset_path=None, prepared_path=None, img_path=None \
            , cluster_path=None, reg_model=None, reg_weight=None \
			, cls_model=None, cls_weight=None):
        if file_path:
            self.file_path =  file_path
        else:
            self.file_path = os.path.dirname(os.path.dirname \
                (os.path.abspath(__file__)))

        if img_path:
            self.img_path = img_path 
        else:
            self.img_path = os.path.join(self.file_path, 'images') 

        if source_path:
            self.source_path = source_path
        else:
            self.source_path = os.path.join(self.file_path, 'resource')

        if dataset_path:
            self.dataset_path = dataset_path
        else:
            self.dataset_path = os.path.join(self.source_path, 'dataset')
        if not os.path.exists( self.dataset_path ):
            os.mkdir(self.dataset_path)

        self.data_frame_path =  os.path.join(self.source_path, 'data_frame_' + str(data_frame).zfill(3)) 
        if not os.path.exists( self.data_frame_path ):
            os.mkdir(self.data_frame_path)

        if prepared_path:
            self.prepared_path = prepared_path 
        else:
            self.prepared_path = os.path.join(self.data_frame_path, 'prepared_data') 
        if not os.path.exists( self.prepared_path ):
            os.mkdir(self.prepared_path)

        if cluster_path:
            self.cluster_path = cluster_path 
        else:
            self.cluster_path = os.path.join(self.data_frame_path, 'clutering') 
        if not os.path.exists( self.cluster_path ):
            os.mkdir(self.cluster_path)

        self.snap_path = os.path.join(self.file_path, 'snapshots') 
        if not os.path.exists( self.snap_path ):
            os.mkdir(self.snap_path)

        if reg_model:
            self.reg_model = reg_model 
        else:
            self.reg_model = os.path.join(self.snap_path, 'reg_model') 
        if not os.path.exists( self.reg_model ):
            os.mkdir(self.reg_model)

        if reg_weight:
            self.reg_weight = reg_weight
        else:
            self.reg_weight = os.path.join(self.snap_path, 'reg_weight')
        if not os.path.exists( self.reg_weight ):
            os.mkdir(self.reg_weight)

        if cls_model:
            self.cls_model = cls_model
        else:
            self.cls_model = os.path.join(self.snap_path, 'cls_model')
        if not os.path.exists( self.cls_model ):
            os.mkdir(self.cls_model)

        if cls_weight:
            self.cls_weight = cls_weight
        else:
            self.cls_weight = os.path.join(self.snap_path, 'cls_weight')           
        if not os.path.exists( self.cls_weight ):
            os.mkdir(self.cls_weight)

if __name__ == '__main__':
    data_frame = 30
    dtw_threshold = 1.40
    config = Config('Close', data_frame, dtw_threshold, ConfigPath(data_frame = data_frame)) 
    sp = StockCluster(config)
    # sp.load_data()
    # d = sp.prepare_data()
    # sp.run_cluster()
    # sp.test_cluster()
    sp.load_cluster()
    # pprint.pprint(sp.cluster_data)
    # sp.visualize_cluster(show=False, save=True)
    # pprint.pprint(sp.dict_data)
    # x_input = np.append(x, yhat)
    # print(x_input)

    # plt.plot()