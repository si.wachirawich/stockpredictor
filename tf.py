from tensorflow.python.client import device_lib
import tensorflow as tf
def get_available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']

print (tf.Session(config=tf.ConfigProto(log_device_placement=True)))


hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello))

print('tf version =' , tf.__version__)
with tf.device('/gpu:0'):
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)

with tf.Session() as sess:
    print (sess.run(c))